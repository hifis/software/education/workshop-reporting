# License Information

All used licenses can be found in the `Licenses` folder.

* The files in the `reports` folder are licensed under [ODBL](licenses/odbl.md).
* Any other files, unless specified otherwise is licensed under [CC-BY-4.0](licenses/cc-by-4.0.md).
* Individual exceptions are stored in license files, calles `<file_name>.license`
