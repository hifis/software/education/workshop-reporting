# Workshop Reporting

This repository oranizes the reporting of HIFIS software/education workshops.

### Contents
  * Reporting Guidelines
  * Actual raw data reported

## Reporting Guidelines

The following guidelines are intended to unify the interpretation of wording and procedure in an effort to keep post-workshop reports comparable and facilitate the generation of statistics.

### Reported Values

The following values are reported on a per-workshop basis if available.
Do not include fields in the reporting for which there are no data.


* **Slug:** A token of the form `YYYY-MM-DD-center` where `center` is the three or four letter abbreviation of the organizing center in lower-case. 
  The slug is used as the report's file name, together with the ending `.yml`
  The slug does not need be repeated in the data itself.
  For multi-day workshops the slug holds the first day of the workshop.
  _Example:_ `2020-02-30-hzdr.yml`
  In case two workshops happen on the same day and from the same center, an optional 0-based counter may be added to the slug.
  _Example:_ `2020-01-31-dlr-0.yml`
* **Dates:** Lists all the day of the workshop as `YYYY-MM-DD`
* **Center:** The official abbreviations of the organizing and executing centers in lower-case.
* **Duration:** The total workshop duration in hours. A full day workshop would be _8_ hours.
* **Links:** A list of URLs associated with the workshop. Please add the main page as first entry.
* **Tags:** A list of lower case keywords associated with the workshop
* **Demand:** The number of people who expressed interest in participating in the specific workshop through direct actions (e.g., registering)
* **Capacity:** The number of persons that are planned to be accomodated by a given workshop
* **Selected:** The number of persons who were assigned a seat in the workshop
* **Attendees:** The number of people who showed up to the workshop from this organization
* **Net Promoter Score:** The NPS value determined for the workshop. (optional field)
  * _score_ indicates the actual NPS score.
  * _n_ indicates the number of attendees that answered the related NPS question. 
* **Status:** Indicates the final situation of the event.
  If the field is not given, it is assumed to be _done_. 
  The status is either
  * _done_ - for events that have been successfully completed
  * _cancelled_ - for events that have been organized and have reportable values associated with them but were not held for any reason
* **Remarks:** A list of strings for free text comments on the workshop. Each entry should be a self-contained thought.

#### Calculating the NPS

The _Net Promoter Score_ is the collected answers to the question `How likely (on a scale of 1 - very unlikely to 10 - very likely) would you recommend this workshop to a colleague?`.
For the calculation of the score calculate `(count(answers >= 9) - count(answers <= 6)) / count(answers) * 100` and report as the closest whole number.
The range of the NPS thus is in the range of -100 (All answers are detractors) to 100 (All answers are promoters).
For weigthing the score in statistical evaluation, the overall count os answers `n` is also reported.

### Derived Values

These values are computed from the reported values and serve as performance indicators.

* **Coverage** `= Selected/Demand` measures how much of the demand could be covered by the workshop. 
  A low coverage is an indicator that more workshops of this kind are needed.
* **No-show-rate** `= (Selected - Attendees)/Selected` determines the ratio of participants that did not show up albeit being allotted a seat in the workshop.
* **Booking** `= Selected/Capacity` indicates the degree of over-promising available workshop resources. The degree of overbooking can also be interpreted as the risk of quality deterioration taken, since too many participants will affect the workshop quality negatively.
* **Utilization** `= Attendees/Capacity` shows how many of the plannes resources were actually used in the workshop.

